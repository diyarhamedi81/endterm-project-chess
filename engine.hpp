#ifndef CHESS_ENGINE_HPP
#define CHESS_ENGINE_HPP
#include "board.hpp"

extern long long int nodeCount;
// values for different kinds of pieces
const int PIECE_VALUES[5] = {900, 500, 325, 300, 100};
// the engine class holds functions for searching & evaluating the positions
class Engine
{
    public:
    Move choose(ChessBoard);
    private:
    int evaluate(ChessBoard&, vector<Move>&);
    int negamax(ChessBoard&, int, int, int);
};

#endif