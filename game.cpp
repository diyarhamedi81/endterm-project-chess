#include "game.hpp"

ChessGame::ChessGame(const string t_fen)
{
    m_stages.push_back(ChessBoard(t_fen));
    m_moves = m_stages.back().generateMoves();
}
ChessGame::~ChessGame()
{
}

ChessBoard ChessGame::getLastBoard()
{
    return m_stages.back();
}
void ChessGame::playMove(Move t_move)
{
    m_stages.push_back(m_stages.back());
    m_stages.back().playMove(t_move);
    m_moves = m_stages.back().generateMoves();
    
}
vector<Move> ChessGame::getMoves()
{
    return m_moves;
}
int ChessGame::getResult()
{
    if (m_moves[0].source == -1)
    {
        if (m_moves[0].destination == 0)
        {
            return -1;
        }
        else if (m_moves[0].destination == 1)
        {
            return 1;
        }
    }
    else if (m_stages.back().getHalfMoveClock() >= 50)
    {
        return -2;
    }
    else if (threefoldRepetition())
    {
        return -3;
    }
    else if (m_stages.back().insufficentMaterial())
    {
        return -4;
    }
    else
    {
        return 0;
    }
}

bool ChessGame::threefoldRepetition()
{
    vector<string> fenList;
    vector<int> fenCount;
    for (int i = 0; i < m_stages.size(); ++i)
    {
        bool newFEN = true;
        string fen = m_stages[i].getFEN(true);
        for (int j = 0; j < fenList.size(); ++j)
        {
            if (fen == fenList[j])
            {
                if (fenCount[j] == 2)
                {
                    return true;
                }
                ++fenCount[j];
                newFEN = false;
                break;
            }
        }
        if (newFEN)
        {
            fenList.push_back(fen);
            fenCount.push_back(1);
        }
    }
    return false;
}