#include "gui.hpp"

int main()
{
    initialize();
    loadResources();
    playScreen();
    return 0;
}