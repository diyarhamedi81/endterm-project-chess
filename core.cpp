#include "core.hpp"
// show the bitboard in binary form, in the shape of the board
void printBitboard(bitboard t_bitboard)
{
    for (int i = 7; i >= 0; --i)
    {
        for (int j = 0; j < 8; ++j)
        {
            cout << ((t_bitboard >> (i * 8 + j)) & 1);
        }
        cout << '\n';
    }
    cout << '\n';
}

int getFirstBit(const bitboard t_bitboard)
{
    return index64[((t_bitboard & -t_bitboard) * 0x07EDD5E59A4E28C2) >> 58];
}

bitboard PAWN_ATTACKS[2][64];
bitboard KNIGHT_MOVES[64];
bitboard KING_MOVES[64];

bitboard shift(const bitboard & t_bitboard, const int t_direction)
{
    if (t_direction < 4)
    {
        return (t_bitboard << SLIDE_DIRECTIONS[t_direction]);
    }
    else
    {
        return (t_bitboard >> SLIDE_DIRECTIONS[t_direction & 3]);
    }
    
}

void initialize()
{
    // generate knight move masks
    const int KNIGHT_DIRECTIONS[8] = {10, 17, 15, 6, -10, -17, -15, -6};
    const int KNIGHT_MOVE_RANKS[8] = {1, 2, 2, 1, -1, -2, -2, -1};
    for (int square = 0; square < 64; ++square)
    {
        //cout << square << ":";
        KNIGHT_MOVES[square] = 0;
        for (int direction = 0; direction < 8; ++direction)
        {
            //cout << (square + KNIGHT_DIRECTIONS[direction]) << "[" << getRank(square + KNIGHT_DIRECTIONS[direction])
            //    << "," << (getRank(square) + KNIGHT_MOVE_RANKS[direction]) << "]";
            if ((getRank(square + KNIGHT_DIRECTIONS[direction]) == getRank(square) + KNIGHT_MOVE_RANKS[direction])
                && (getRank(square + KNIGHT_DIRECTIONS[direction]) >= 0) && (getRank(square + KNIGHT_DIRECTIONS[direction]) <= 7))
            {
                //cout << "Y";
                KNIGHT_MOVES[square] |= (1ULL << (square + KNIGHT_DIRECTIONS[direction]));
            }
            //cout << " ";
        }
        //cout << "\n";
    }
    // generate king move masks
    const int KING_MOVE_RANKS[8] = {0, 1, 1, 1, 0, -1, -1, -1};
    for (int square = 0; square < 64; ++square)
    {
        KING_MOVES[square] = 0;
        for (int direction = 0; direction < 8; ++direction)
        {
            if ((((square + SLIDE_DIRECTIONS[direction]) / 8) == (square / 8) + KING_MOVE_RANKS[direction])
            && (getRank(square + SLIDE_DIRECTIONS[direction]) >= 0) && (getRank(square + SLIDE_DIRECTIONS[direction]) <= 7))
            {
                KING_MOVES[square] |= (1ULL << (square + SLIDE_DIRECTIONS[direction]));
            }
        }
    }
    // generate pawn attack masks
    for (int square = 0; square < 64; ++square)
    {
        PAWN_ATTACKS[WHITE][square] = 0;
        PAWN_ATTACKS[BLACK][square] = 0;
        if (((square + 9) / 8) == (square / 8) + 1)
        {
            PAWN_ATTACKS[WHITE][square] |= (1ULL << (square + 9));
        }
        if (((square + 7) / 8) == (square / 8) + 1)
        {
            PAWN_ATTACKS[WHITE][square] |= (1ULL << (square + 7));
        }
        if (((square - 9) / 8) == (square / 8) - 1)
        {
            PAWN_ATTACKS[BLACK][square] |= (1ULL << (square - 9));
        }
        if (((square - 7) / 8) == (square / 8) - 1)
        {
            PAWN_ATTACKS[BLACK][square] |= (1ULL << (square - 7));
        }
    }
}

int bitCount(bitboard t_bitboard)
{
    int count = 0;
    while (t_bitboard != 0)
    {
        count += (t_bitboard & 1);
        t_bitboard >>= 1;
    }
    return count;
}

int amplifier(const int t_color)
{
    return 1 - 2 * t_color;
}

int getRank(const int t_square)
{
    return t_square >> 3;
}

int getFile(const int t_square)
{
    return t_square & 7;
}

int getDiagonal(const int t_square)
{
    return getRank(t_square) + getFile(t_square);
}

int getAntiDiagonal(const int t_square)
{
    return 7 + getFile(t_square) - getRank(t_square);
}

string getSquareName(const int t_square)
{
    return {(char)(int('a') + getFile(t_square)), (char)(int('1') + getRank(t_square))};
}

int getSquareAddress(string t_square)
{
    return ((t_square[1] - '1') << 4) | (t_square[0] - 'a');
}