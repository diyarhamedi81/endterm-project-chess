#ifndef CHESS_BOARD_HPP
#define CHESS_BOARD_HPP
#include "core.hpp"
struct ChessBoard
{
public:
    // constructor based on FEN format
    ChessBoard(const string = "rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1");
    // constructor based on other board
    ChessBoard(const ChessBoard &);
    ~ChessBoard();
    // show the board on the console
    void print();
    // computes list of the legal moves
    vector<Move> generateMoves();
    void playMove(Move);
    // get FEN format from the board
    string getFEN(bool = false);
    // get bitboard of the specific color & piece
    bitboard getPieces(int, int);
    Color getTurn();
    bool canCastle(int, int);
    int getEnpassent();
    int getHalfMoveClock();
    int getFullMoveCount();
    // checks for insufficent material (if true, the game is drawn)
    bool insufficentMaterial();
private:
    // bitboards keep the information about place of pieces of every color & type
    bitboard m_pieces[2][6];
    bitboard m_allPieces[2];
    bitboard m_emptyPieces;
    int m_kingSquares[2];
    Color m_turn;
    int m_opponent;
    bool m_canCastle[2][2];
    // the square to which en-passent may be possible
    // if not available, it is set to -33 to prevent conflict with black promotion destinations
    int m_enpassentSquare;
    // half move clock, it resets by capturing a piece or moving a pawn
    // once it reaches 50 plies, the game is drawn
    int m_halfMoveClock;
    // full move count, it increases everytime black moves
    // it is used only for FEN formats
    int m_fullMoveCount;
    // generate the legal pawn moves to the square, en-passent & promotion included
    vector<Move> generatePawnMovesTo(const bitboard, const bitboard);
    // generate the legal pawn moves to the square, excluding the pinned ones
    vector<Move> generatePieceMovesTo(bitboard, const bitboard, const int = 8);
    // get sliding path of a bitboard in the specific direction, excluding the pinned ones
    bitboard getSlidingBitboard(bitboard, bitboard, const int);
    // generate the legal moves for the knights, pinned knights cannot move
    vector<Move> generateKnightMoves(const bitboard);
    // generate the legal moves for the sliding pieces(queens, rooks & bishops)
    vector<Move> generateSlidingMoves(const bitboard);
    // generate the legal moves for the pawns, including pinned ones
    vector<Move> generatePawnMoves(const bitboard);
    // generate the legal moves for the pinned pieces
    vector<Move> generatePinnedMoves(const bitboard);
    // generate the legal moves for the king, castling included
    vector<Move> generateKingMoves(bool);
};

#endif
