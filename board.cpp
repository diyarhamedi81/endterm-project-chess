#include "board.hpp"

ChessBoard::ChessBoard(const string t_fenFormat)
{
    for (int piece = 0; piece < 6; ++piece)
    {
        m_pieces[WHITE][piece] = 0;
        m_pieces[BLACK][piece] = 0;
    }
    int square = 56;
    int i;
    for (i = 0; t_fenFormat[i] != ' '; ++i)
    {
        switch (t_fenFormat[i])
        {
        case '/':
            square -= 17;
            break;
        case 'K':
            m_pieces[WHITE][KING] |= (1ULL << square);
            m_kingSquares[0] = square;
            break;
        case 'Q':
            m_pieces[WHITE][QUEEN] |= (1ULL << square);
            break;
        case 'R':
            m_pieces[WHITE][ROOK] |= (1ULL << square);
            break;
        case 'B':
            m_pieces[WHITE][BISHOP] |= (1ULL << square);
            break;
        case 'N':
            m_pieces[WHITE][KNIGHT] |= (1ULL << square);
            break;
        case 'P':
            m_pieces[WHITE][PAWN] |= (1ULL << square);
            break;
        case 'k':
            m_pieces[BLACK][KING] |= (1ULL << square);
            m_kingSquares[1] = square;
            break;
        case 'q':
            m_pieces[BLACK][QUEEN] |= (1ULL << square);
            break;
        case 'r':
            m_pieces[BLACK][ROOK] |= (1ULL << square);
            break;
        case 'b':
            m_pieces[BLACK][BISHOP] |= (1ULL << square);
            break;
        case 'n':
            m_pieces[BLACK][KNIGHT] |= (1ULL << square);
            break;
        case 'p':
            m_pieces[BLACK][PAWN] |= (1ULL << square);
            break;
        default:
            square += (t_fenFormat[i] - '0' - 1);
            break;
        }
        ++square;
    }
    m_allPieces[WHITE] = 0;
    m_allPieces[BLACK] = 0;
    for (int piece = 0; piece < 6; ++piece)
    {
        m_allPieces[WHITE] |= m_pieces[WHITE][piece];
        m_allPieces[BLACK] |= m_pieces[BLACK][piece];
    }
    m_emptyPieces = ~(m_allPieces[WHITE] | m_allPieces[BLACK]);

    ++i;
    m_turn = (t_fenFormat[i] == 'w') ? WHITE : BLACK;
    m_opponent = m_turn ^ 1;

    i += 2;
    m_canCastle[0][0] = (t_fenFormat[i] == 'K');
    m_canCastle[0][1] = (t_fenFormat[i] == 'Q');
    m_canCastle[1][0] = (t_fenFormat[i] == 'k');
    m_canCastle[1][1] = (t_fenFormat[i] == 'q');
    while (t_fenFormat[i] != ' ')
    {
        ++i;
        switch (t_fenFormat[i])
        {
        case 'Q':
            m_canCastle[0][1] = true;
            break;
        case 'k':
            m_canCastle[1][0] = true;
            break;
        case 'q':
            m_canCastle[1][1] = true;
            break;
        default:
            break;
        }
    }

    ++i;
    if (t_fenFormat[i] != '-')
    {
        m_enpassentSquare = (t_fenFormat[i] - 'a') + ((t_fenFormat[i + 1] - '1') << 3);
        ++i;
    }
    else
    {
        m_enpassentSquare = -33;
    }

    i += 2;
    std::stringstream nMoves(t_fenFormat.substr(i, t_fenFormat.size()));
    nMoves >> m_halfMoveClock >> m_fullMoveCount;
}

ChessBoard::ChessBoard(const ChessBoard &t_other)
    : m_turn(t_other.m_turn), m_opponent(t_other.m_opponent), m_enpassentSquare(t_other.m_enpassentSquare),
      m_halfMoveClock(t_other.m_halfMoveClock), m_fullMoveCount(t_other.m_fullMoveCount)
{
    for (int piece = 0; piece < 6; ++piece)
    {
        m_pieces[WHITE][piece] = t_other.m_pieces[WHITE][piece];
        m_pieces[BLACK][piece] = t_other.m_pieces[BLACK][piece];
    }
    m_allPieces[WHITE] = t_other.m_allPieces[WHITE];
    m_allPieces[BLACK] = t_other.m_allPieces[BLACK];
    m_emptyPieces = t_other.m_emptyPieces;
    m_kingSquares[WHITE] = t_other.m_kingSquares[WHITE];
    m_kingSquares[BLACK] = t_other.m_kingSquares[BLACK];
    m_canCastle[WHITE][KING] = t_other.m_canCastle[WHITE][KING];
    m_canCastle[WHITE][QUEEN] = t_other.m_canCastle[WHITE][QUEEN];
    m_canCastle[BLACK][KING] = t_other.m_canCastle[BLACK][KING];
    m_canCastle[BLACK][QUEEN] = t_other.m_canCastle[BLACK][QUEEN];
}

ChessBoard::~ChessBoard()
{
}

void ChessBoard::print()
{
    cout << "Board:\n"
    << "  +---+---+---+---+---+---+---+---+\n";
    for (int rank = 7; rank >= 0; --rank)
    {
        cout << (rank + 1) << " |";
        for (int file = 0; file < 8; ++file)
        {
            string pieceChar = " ";
            for (int piece = 0; piece < 6; ++piece)
            {
                if (m_pieces[WHITE][piece] & (1ULL << (rank * 8 + file)))
                {
                    pieceChar = PIECE_SYMBOLS[piece];
                    break;
                }
                else if (m_pieces[BLACK][piece] & (1ULL << (rank * 8 + file)))
                {
                    pieceChar = PIECE_SYMBOLS[6 + piece];
                    break;
                }
            }
            cout << " " << pieceChar << " |";
        }
        cout << "\n  +---+---+---+---+---+---+---+---+\n";
    }
    cout << "    a   b   c   d   e   f   g   h\n";
    /*
    cout << "Turn: " << ((m_turn == 0) ? "White" : "Black") << "\n";
    cout << "Can white castle king side: " << (m_canCastle[0][0] ? "Yes" : "No") << "\n";
    cout << "Can white castle queen side: " << (m_canCastle[0][1] ? "Yes" : "No") << "\n";
    cout << "Can black castle king side: " << (m_canCastle[1][0] ? "Yes" : "No") << "\n";
    cout << "Can black castle queen side: " << (m_canCastle[1][1] ? "Yes" : "No") << "\n";
    cout << "En passent square: " << ((m_enpassentSquare != -33) ? getSquareName(m_enpassentSquare) : "None") << "\n";
    cout << "Half move clock: " << m_halfMoveClock << "\nFull move count: " << m_fullMoveCount << "\n";
    */
}

bitboard ChessBoard::getSlidingBitboard(bitboard t_bitboard, bitboard t_empty, const int t_direction)
{
    t_empty &= SHIFT_MASKS[t_direction];
    for (int i = 0; i < 6; ++i)
    {
        t_bitboard |= t_empty & shift(t_bitboard, t_direction);
    }
    //printBitboard(SHIFT_MASKS[t_direction] & shift(t_bitboard, t_direction));
    return SHIFT_MASKS[t_direction] & shift(t_bitboard, t_direction);
}

vector<Move> ChessBoard::generatePawnMovesTo(const bitboard t_bitboard, const bitboard t_pinned)
{
    vector<Move> moves;
    int square = getFirstBit(t_bitboard);
    bitboard pawns = m_pieces[m_turn][PAWN];
    // should moves be capture moves?
    if ((t_bitboard & m_allPieces[m_opponent]) != 0)
    {
        pawns &= PAWN_ATTACKS[m_opponent][square];
        for (int direction = 1; direction < 4; direction += 2)
        {
            bitboard pawn = pawns & shift(t_bitboard, (m_opponent << 2) + direction);
            if (pawn != 0)
            {
                bool validMove = true;
                if ((pawn & t_pinned) != 0)
                {
                    validMove = false;
                    if (direction == 1)
                    {
                        if (getAntiDiagonal(m_kingSquares[m_turn]) == getAntiDiagonal(getFirstBit(pawn)))
                        {
                            validMove = true;
                        }
                    }
                    else
                    {
                        if (getDiagonal(m_kingSquares[m_turn]) == getDiagonal(getFirstBit(pawn)))
                        {
                            validMove = true;
                        }
                    }
                }
                if (validMove)
                {
                    if (getRank(square) == (7 * m_opponent))
                    {
                        for (int i = 0; i < 4; ++i)
                        {
                            moves.push_back(Move{square + SLIDE_DIRECTIONS[(m_opponent << 2) + direction]
                                , square + PAWN_PROMOTIONS[m_turn][i]});
                        }
                    }
                    else
                    {
                        moves.push_back(Move{square + SLIDE_DIRECTIONS[(m_opponent << 2) + direction], square});
                    }
                }
            }
        }
    }
    // check for enpassent move
    else if (square == m_enpassentSquare)
    {
        pawns &= PAWN_ATTACKS[m_opponent][square];
        while (pawns != 0)
        {
            bitboard pawnMove = (pawns & -pawns) | t_bitboard;
            m_pieces[m_turn][PAWN] ^= pawnMove;
            m_pieces[m_opponent][PAWN] &= ~shift(t_bitboard, 2 + 4 * m_opponent);
            bitboard newEmpty = (m_emptyPieces ^ pawnMove) | shift(t_bitboard, 2 + 4 * m_opponent);
            bitboard attackers = 0;
            bitboard orthogonalAttackers = m_pieces[m_opponent][QUEEN] | m_pieces[m_opponent][ROOK];
            bitboard diagonalAttackers = m_pieces[m_opponent][QUEEN] | m_pieces[m_opponent][BISHOP];
            for (int direction = 0; direction < 8; ++direction)
            {
                attackers |= getSlidingBitboard(m_pieces[m_turn][KING], newEmpty, direction)
                    & (((direction & 1) == 0) ? orthogonalAttackers : diagonalAttackers);
            }
            if (attackers == 0)
            {
                moves.push_back(Move{getFirstBit(pawns), square});
            }
            m_pieces[m_turn][PAWN] ^= pawnMove;
            m_pieces[m_opponent][PAWN] |= ((m_turn == WHITE) ? (t_bitboard >> 8) : (t_bitboard << 8));
            pawns &= pawns - 1;
        }
    }
    // is square empty for pawn push
    else if ((t_bitboard & m_emptyPieces) != 0)
    {
        // forward push
        bitboard pawn = ((m_turn == WHITE) ? (t_bitboard >> 8) : (t_bitboard << 8));
        if ((pawn & pawns) != 0)
        {
            bool validMove = true;
            if ((pawn & t_pinned) && (getFile(getFirstBit(pawn)) != getFile(m_kingSquares[m_turn])))
            {
                validMove = false;
            }
            if (validMove)
            {
                if (getRank(square) == (7 * m_opponent))
                {
                    for (int i = 0; i < 4; ++i)
                    {
                        moves.push_back(Move{(m_turn == WHITE) ? (square - 8) : (square + 8)
                            , square + PAWN_PROMOTIONS[m_turn][i]});
                    }
                }
                else
                {
                    moves.push_back(Move{(m_turn == WHITE) ? (square - 8) : (square + 8), square});
                }
            }
        }
        // double push
        else if ((getRank(square) == (3 + m_turn)) && ((pawn & m_emptyPieces) != 0))
        {
            pawn = ((m_turn == WHITE) ? (pawn >> 8) : (pawn << 8));
            if ((pawn & pawns) != 0)
            {
                bool validMove = true;
                if ((pawn & t_pinned) && (getFile(getFirstBit(pawn)) != getFile(m_kingSquares[m_turn])))
                {
                    validMove = false;
                }
                if (validMove)
                {
                    moves.push_back(Move{(m_turn == WHITE) ? (square - 16) : (square + 16), square});
                }
            }
        }

    }
    return moves;
}

vector<Move> ChessBoard::generatePieceMovesTo(bitboard t_bitboard, const bitboard t_pinned
    , const int t_direction)
{
    vector<Move> moves;
    int square = getFirstBit(t_bitboard);
    // generate knight moves
    bitboard knights = m_pieces[m_turn][KNIGHT] & KNIGHT_MOVES[square] & ~t_pinned;
    while (knights != 0)
    {
        moves.push_back(Move{getFirstBit(knights), square});
        knights &= knights - 1;
    }
    //generate sliding moves
    bitboard orthogonal = (m_pieces[m_turn][QUEEN] | m_pieces[m_turn][ROOK]) & ~t_pinned;
    bitboard diagonal = (m_pieces[m_turn][QUEEN] | m_pieces[m_turn][BISHOP]) & ~t_pinned;
    for (int direction = 0; direction < 8; ++direction)
    {
        if ((direction & 11) != (t_direction & 11))
        {
            bitboard pieces = getSlidingBitboard(t_bitboard, m_emptyPieces, direction)
                & (((direction & 1) == 0) ? orthogonal : diagonal);
            if (pieces != 0)
            {
                moves.push_back(Move{getFirstBit(pieces), square});
            }
        }
    }
    vector<Move> pawnMoves;
    if (getFirstBit(shift(t_bitboard, 2 + 4 * m_turn)) == m_enpassentSquare)
    {
        t_bitboard = shift(t_bitboard, 2 + 4 * m_turn);
    }
    pawnMoves = generatePawnMovesTo(t_bitboard, t_pinned);
    moves.insert(moves.end(), pawnMoves.begin(), pawnMoves.end());
    return moves;
}

vector<Move> ChessBoard::generateKnightMoves(const bitboard t_pinned)
{
    vector<Move> moves;
    bitboard knights = m_pieces[m_turn][KNIGHT] & ~t_pinned;
    while (knights != 0)
    {
        int knightSquare = getFirstBit(knights);
        // generate the available squares for the knight
        bitboard moveBitboard = KNIGHT_MOVES[knightSquare] & ~m_allPieces[m_turn];
        while (moveBitboard != 0)
        {
            moves.push_back(Move{knightSquare, getFirstBit(moveBitboard)});
            moveBitboard &= moveBitboard - 1;
        }
        knights &= knights - 1;
    }
    return moves;
}

vector<Move> ChessBoard::generateSlidingMoves(const bitboard t_pinned)
{
    vector<Move> moves;
    bitboard orthogonal = (m_pieces[m_turn][QUEEN] | m_pieces[m_turn][ROOK]) & ~t_pinned;
    bitboard diagonal = (m_pieces[m_turn][QUEEN] | m_pieces[m_turn][BISHOP]) & ~t_pinned;
    for (int direction = 0; direction < 8; ++direction)
    {
        bitboard moveSquares = getSlidingBitboard(((direction & 1) == 0) ? orthogonal : diagonal
            , m_emptyPieces, direction) & ~m_allPieces[m_turn];
        //printBitboard(m_emptyPieces);
        //printBitboard(moveSquares);
        //printBitboard(SHIFT_MASKS[direction]);
        bitboard remainingPieces = ((direction & 1) == 0) ? orthogonal : diagonal;
        while (remainingPieces != 0)
        {
            int source = getFirstBit(remainingPieces);
            int destination = source + SLIDE_DIRECTIONS[direction];
            while (((1ULL << destination) & moveSquares) != 0)
            {
                moves.push_back(Move{source, destination});
                destination += SLIDE_DIRECTIONS[direction];
            }
            remainingPieces &= remainingPieces - 1;
        }
    }
    return moves;
}

vector<Move> ChessBoard::generatePawnMoves(const bitboard t_pinned)
{
    vector<Move> moves;
    bitboard pushBitboard = 0;
    // destination for normal push
    pushBitboard |= shift(m_pieces[m_turn][PAWN], (m_turn << 2) | 2) & m_emptyPieces;
    // adds destination for double push
    pushBitboard |= shift((pushBitboard & RANK_MASKS[2 + 3 * m_turn]), (m_turn << 2) | 2) & m_emptyPieces;
    // destination for pawn captures
    bitboard captureBitboard = 0;
    captureBitboard |= shift(m_pieces[m_turn][PAWN], 1 + m_turn * 6) & SHIFT_MASKS[0];
    captureBitboard |= shift(m_pieces[m_turn][PAWN], 3 + m_turn * 2) & SHIFT_MASKS[4];
    captureBitboard &= m_allPieces[m_opponent];
    //printBitboard(pushBitboard);
    if (m_enpassentSquare != -33)
    {
        captureBitboard |= 1ULL << m_enpassentSquare;
    }
    while (pushBitboard != 0)
    {
        vector<Move> pawnMoves = generatePawnMovesTo(pushBitboard & -pushBitboard, t_pinned);
        moves.insert(moves.end(), pawnMoves.begin(), pawnMoves.end());
        pushBitboard &= pushBitboard - 1;
    }
    while (captureBitboard != 0)
    {
        vector<Move> pawnMoves = generatePawnMovesTo(captureBitboard & -captureBitboard, t_pinned);
        moves.insert(moves.end(), pawnMoves.begin(), pawnMoves.end());
        captureBitboard &= captureBitboard - 1;
    }
    return moves;
}

vector<Move> ChessBoard::generatePinnedMoves(const bitboard t_pinned)
{
    vector<Move> moves;
    bitboard orthogonal = (m_pieces[m_turn][QUEEN] | m_pieces[m_turn][ROOK]) & t_pinned;
    bitboard diagonal = (m_pieces[m_turn][QUEEN] | m_pieces[m_turn][BISHOP]) & t_pinned;
    while (orthogonal != 0)
    {
        bitboard pinRay = 0;
        int pinnedSquare = getFirstBit(orthogonal);
        if (getRank(pinnedSquare) == getRank(m_kingSquares[m_turn]))
        {
            for (int direction = 0; direction < 8; direction += 4)
            {
                int destination = pinnedSquare + SLIDE_DIRECTIONS[direction];
                while (((1ULL << destination) & m_emptyPieces) != 0)
                {
                    moves.push_back(Move{pinnedSquare, destination});
                    destination += SLIDE_DIRECTIONS[direction];
                }
                if (((1ULL << destination) & m_allPieces[m_opponent]) != 0)
                {
                    moves.push_back(Move{pinnedSquare, destination});
                }
            }
        }
        else if (getFile(pinnedSquare) == getFile(m_kingSquares[m_turn]))
        {
            for (int direction = 2; direction < 8; direction += 4)
            {
                int destination = pinnedSquare + SLIDE_DIRECTIONS[direction];
                while (((1ULL << destination) & m_emptyPieces) != 0)
                {
                    moves.push_back(Move{pinnedSquare, destination});
                    destination += SLIDE_DIRECTIONS[direction];
                }
                if (((1ULL << destination) & m_allPieces[m_opponent]) != 0)
                {
                    moves.push_back(Move{pinnedSquare, destination});
                }
            }
        }
        orthogonal &= orthogonal - 1;
    }
    while (diagonal != 0)
    {
        bitboard pinRay = 0;
        int pinnedSquare = getFirstBit(diagonal);
        if (getDiagonal(pinnedSquare) == getDiagonal(m_kingSquares[m_turn]))
        {
            for (int direction = 3; direction < 8; direction += 4)
            {
                int destination = pinnedSquare + SLIDE_DIRECTIONS[direction];
                while (((1ULL << destination) & m_emptyPieces) != 0)
                {
                    moves.push_back(Move{pinnedSquare, destination});
                    destination += SLIDE_DIRECTIONS[direction];
                }
                if (((1ULL << destination) & m_allPieces[m_opponent]) != 0)
                {
                    moves.push_back(Move{pinnedSquare, destination});
                }
            }
        }
        else if (getAntiDiagonal(pinnedSquare) == getAntiDiagonal(m_kingSquares[m_turn]))
        {
            for (int direction = 1; direction < 8; direction += 4)
            {
                int destination = pinnedSquare + SLIDE_DIRECTIONS[direction];
                while (((1ULL << destination) & m_emptyPieces) != 0)
                {
                    moves.push_back(Move{pinnedSquare, destination});
                    destination += SLIDE_DIRECTIONS[direction];
                }
                if (((1ULL << destination) & m_allPieces[m_opponent]) != 0)
                {
                    moves.push_back(Move{pinnedSquare, destination});
                }
            }
        }
        diagonal &= diagonal - 1;
    }
    return moves;
}

vector<Move> ChessBoard::generateKingMoves(bool t_check)
{
    vector<Move> moves;
    // compute squares which are illegal for the king
    // we add own pieces and opponent king area at first, as they are trivial
    bitboard illegal = m_allPieces[m_turn] | KING_MOVES[m_kingSquares[m_opponent]];
    // generate squares endangered by opponent pawns
    illegal |= shift(m_pieces[m_opponent][PAWN], 7 - m_turn * 6) & SHIFT_MASKS[0];
    illegal |= shift(m_pieces[m_opponent][PAWN], 5 - m_turn * 2) & SHIFT_MASKS[4];
    bitboard knights = m_pieces[m_opponent][KNIGHT];
    while (knights != 0)
    {
        // generate squares endangered by opponent knights
        illegal |= KNIGHT_MOVES[getFirstBit(knights)];
        knights &= knights - 1;
    }
    // generate squares endangered by opponent sliders
    bitboard orthogonal = (m_pieces[m_opponent][QUEEN] | m_pieces[m_opponent][ROOK]);
    bitboard diagonal = (m_pieces[m_opponent][QUEEN] | m_pieces[m_opponent][BISHOP]);
    for (int direction = 0; direction < 8; ++direction)
    {
        illegal |= getSlidingBitboard(((direction & 1) == 0) ? orthogonal : diagonal
            , m_emptyPieces | m_pieces[m_turn][KING], direction);
    }
    bitboard available = KING_MOVES[m_kingSquares[m_turn]] & ~illegal;
    
    while (available != 0)
    {
        moves.push_back(Move{m_kingSquares[m_turn], getFirstBit(available)});
        available &= available - 1;
    }
    // 3 criteria are checked for castle moves
    // 1.The king should not be in check
    // 2.The player has the right to castle
    // -If one of the rooks have moved before, castling on the side of the rook is illegal
    // -If the king has moves before, castling is completely illegal
    // 3.the squares in the way of castling are empty and not attacked by opponent's pieces
    if (!t_check)
    {
        for (int i = 0; i < 2; ++i)
        {
            if ((m_canCastle[m_turn][i])
                && (((CASTLE_CHECK[m_turn][i] & illegal) | (CASTLE_EMPTY[m_turn][i] & ~m_emptyPieces)) == 0))
            {
                moves.push_back(Move{m_kingSquares[m_turn], CASTLE_DESTINATIONS[m_turn][i]});
            }
        }
    }
    return moves;
}

vector<Move> ChessBoard::generateMoves()
{
    vector<Move> moves;
    bitboard attackers = 0;
    bool canBlockCheck = true;
    // check if any knight or pawn is giving check
    // in that case, it's not possible to block the check
    attackers |= m_pieces[m_opponent][KNIGHT] & KNIGHT_MOVES[m_kingSquares[m_turn]];
    attackers |= m_pieces[m_opponent][PAWN] & PAWN_ATTACKS[m_turn][m_kingSquares[m_turn]];
    if (attackers != 0)
    {
        canBlockCheck = false;
    }
    bitboard orthogonalAttackers = m_pieces[m_opponent][QUEEN] | m_pieces[m_opponent][ROOK];
    bitboard diagonalAttackers = m_pieces[m_opponent][QUEEN] | m_pieces[m_opponent][BISHOP];
    for (int direction = 0; direction < 8; ++direction)
    {
        attackers |= getSlidingBitboard(m_pieces[m_turn][KING], m_emptyPieces, direction)
            & (((direction & 1) == 0) ? orthogonalAttackers : diagonalAttackers);
    }
    // compute pinned pieces
    bitboard pinnedPieces = 0;
    // ignore own pieces for pin check
    bitboard noOwnPieces = m_emptyPieces | m_allPieces[m_turn];
    for (int direction = 0; direction < 8; ++direction)
    {
        // check for pinners
        bitboard pinRay = getSlidingBitboard(m_pieces[m_turn][KING], noOwnPieces , direction);
        bitboard pinner = pinRay & (((direction & 1) == 0) ? orthogonalAttackers : diagonalAttackers);
        if (pinner != 0)
        {
            // possible squares for pinned pieces
            bitboard possiblePins;
            if ((direction >> 2) == 0)
            {
                possiblePins = (pinner - m_pieces[m_turn][KING]) & pinRay;
            }
            else
            {
                possiblePins = (m_pieces[m_turn][KING] - pinner) & pinRay;
            }
            possiblePins &= m_allPieces[m_turn];
            // is only one own piece in pin ray
            if ((possiblePins & (possiblePins - 1)) == 0)
            {
                pinnedPieces |= possiblePins;
            }
        }
    }
    // compute king moves, including castles
    vector<Move> kingMoves = generateKingMoves(attackers != 0);
    moves.insert(moves.end(), kingMoves.begin(), kingMoves.end());
    // no piece is giving check, so move generation is normal
    if (attackers == 0)
    {
        // compute pawn moves, including pinned pawns, en passents and promotions
        vector<Move> pawnMoves = generatePawnMoves(pinnedPieces);
        moves.insert(moves.end(), pawnMoves.begin(), pawnMoves.end());
        // compute knight moves, pinned knights cannot move
        vector<Move> knightMoves = generateKnightMoves(pinnedPieces);
        moves.insert(moves.end(), knightMoves.begin(), knightMoves.end());
        // compute sliding moves, excluding pinned pieces
        vector<Move> slidingMoves = generateSlidingMoves(pinnedPieces);
        moves.insert(moves.end(), slidingMoves.begin(), slidingMoves.end());
        // compute pinned sliding moves
        vector<Move> pinnedMoves = generatePinnedMoves(pinnedPieces);
        moves.insert(moves.end(), pinnedMoves.begin(), pinnedMoves.end());
    }
    // only one piece is giving check, so it might be possible to capture it
    else if ((attackers  & (attackers - 1)) == 0)
    {
        vector<Move> captureMoves = generatePieceMovesTo(attackers, pinnedPieces);
        moves.insert(moves.end(), captureMoves.begin(), captureMoves.end());
        // if the check giving piece is a slider, try to block the check
        // by generating all the moves to the squares between the piece and the king
        if (canBlockCheck)
        {
            // possible squares to block the check
            bitboard possibleBlockSquares;
            bitboard checkRay;
            int checkDirection;
            int attackerSquare = getFirstBit(attackers);
            // is attack on a rank?
            if (getRank(m_kingSquares[m_turn]) == getRank(attackerSquare))
            {
                checkRay = RANK_MASKS[getRank(attackerSquare)];
                checkDirection = 0;
            }
            // is attack on a file?
            if (getFile(m_kingSquares[m_turn]) == getFile(attackerSquare))
            {
                checkRay = FILE_MASKS[getFile(attackerSquare)];
                checkDirection = 2;
            }
            // is attack on a diagonal?
            if (getDiagonal(m_kingSquares[m_turn]) == getDiagonal(attackerSquare))
            {
                checkRay = DIAG_MASKS[getDiagonal(attackerSquare)];
                checkDirection = 3;
            }
            // is attack on an anti diagonal?
            if (getAntiDiagonal(m_kingSquares[m_turn]) == getAntiDiagonal(attackerSquare))
            {
                checkRay = ANTI_DIAG_MASKS[getAntiDiagonal(attackerSquare)];
                checkDirection = 1;
            }
            if (attackers > m_pieces[m_turn][KING])
            {
                possibleBlockSquares = attackers - m_pieces[m_turn][KING];
            }
            else
            {
                possibleBlockSquares = m_pieces[m_turn][KING] - attackers;
            }
            possibleBlockSquares &= checkRay & ~(m_pieces[m_turn][KING] | attackers);
            while (possibleBlockSquares != 0)
            {
                bitboard possibleBlockSquare = possibleBlockSquares & -possibleBlockSquares;
                vector<Move> blockMoves = generatePieceMovesTo(possibleBlockSquare, pinnedPieces);
                moves.insert(moves.end(), blockMoves.begin(), blockMoves.end());
                possibleBlockSquares &= possibleBlockSquares - 1;
            }
        }
    }
    if (moves.size() == 0)
    {
        moves.push_back(Move{-1, (attackers == 0) ? 0 : 1});
    }
    return moves;
}

void ChessBoard::playMove(Move t_move)
{
    bitboard sourceBitboard = 1ULL << t_move.source;
    ++m_halfMoveClock;
    if (m_turn == BLACK)
    {
        ++m_fullMoveCount;
    }
    if (t_move.destination >= 64)
    {
        m_halfMoveClock = 0;
        m_enpassentSquare = -33;
        int promotionPiece = 0;
        for (; t_move.destination >= 64 ; ++promotionPiece)
        {
            t_move.destination -= 8;
        }
        bitboard destinationBitboard = 1ULL << t_move.destination;
        m_pieces[m_turn][PAWN] &= ~sourceBitboard;
        m_allPieces[m_turn] &= ~sourceBitboard;
        m_pieces[m_turn][promotionPiece] |= destinationBitboard;
        m_allPieces[m_turn] |= destinationBitboard;
        m_allPieces[m_opponent] &= ~destinationBitboard;
                for (int piece = 1; piece < 5; ++piece)
        {
            if ((m_pieces[m_opponent][piece] & destinationBitboard) != 0)
            {
                m_halfMoveClock = 0;
                m_pieces[m_opponent][piece] &= ~destinationBitboard;
                break;
            }
        }
    }
    else if (t_move.destination <= -1)
    {
        m_halfMoveClock = 0;
        m_enpassentSquare = -33;
        int promotionPiece = 0;
        for (; t_move.destination <= -1 ; ++promotionPiece)
        {
            t_move.destination += 8;
        }
        bitboard destinationBitboard = 1ULL << t_move.destination;
        m_pieces[m_turn][PAWN] &= ~sourceBitboard;
        m_allPieces[m_turn] &= ~sourceBitboard;
        m_pieces[m_turn][promotionPiece] |= destinationBitboard;
        m_allPieces[m_turn] |= destinationBitboard;
        m_allPieces[m_opponent] &= ~destinationBitboard;
        for (int piece = 1; piece < 5; ++piece)
        {
            if ((m_pieces[m_opponent][piece] & destinationBitboard) != 0)
            {
                m_halfMoveClock = 0;
                m_pieces[m_opponent][piece] &= ~destinationBitboard;
                break;
            }
        }
    }
    else 
    {
        bitboard destinationBitboard = 1ULL << t_move.destination;
        if ((t_move.destination == m_enpassentSquare) && ((m_pieces[m_turn][PAWN] & sourceBitboard) != 0))
        {
            m_enpassentSquare = -33;
            m_pieces[m_turn][PAWN] &= ~sourceBitboard;
            m_pieces[m_turn][PAWN] |= destinationBitboard;
            m_allPieces[m_turn] &= ~sourceBitboard;
            m_allPieces[m_turn] |= destinationBitboard;
            m_pieces[m_opponent][PAWN] &= ~(shift(destinationBitboard, (m_turn == WHITE) ? 6 : 2));
            m_allPieces[m_opponent] &= ~(shift(destinationBitboard, (m_turn == WHITE) ? 6 : 2));
            m_halfMoveClock = 0;    
        }
        else
        {
            m_enpassentSquare = -33;
            if (t_move.source == m_kingSquares[m_turn])
            {
                m_canCastle[m_turn][0] = false;
                m_canCastle[m_turn][1] = false;
                if (t_move.source == (56 * m_turn + 4))
                {
                    if (t_move.destination == CASTLE_DESTINATIONS[m_turn][0])
                    {
                        m_pieces[m_turn][ROOK] ^= (m_pieces[m_turn][KING] << 3) | (m_pieces[m_turn][KING] << 1);
                        m_allPieces[m_turn] ^= (m_pieces[m_turn][KING] << 3) | (m_pieces[m_turn][KING] << 1);
                    }
                    else if (t_move.destination == CASTLE_DESTINATIONS[m_turn][1])
                    {
                        m_pieces[m_turn][ROOK] ^= (m_pieces[m_turn][KING] >> 4) | (m_pieces[m_turn][KING] >> 1);
                        m_allPieces[m_turn] ^= (m_pieces[m_turn][KING] >> 4) | (m_pieces[m_turn][KING] >> 1);
                    }    
                }
                m_kingSquares[m_turn] = t_move.destination;
            }
            else if (t_move.source == ((m_turn * 56) | 7))
            {
                m_canCastle[m_turn][0] = false;
            }
            else if (t_move.source == (m_turn * 56))
            {
                m_canCastle[m_turn][1] = false;
            }
            for (int piece = 0; piece < 6; ++piece)
            {
                if ((m_pieces[m_turn][piece] & sourceBitboard) != 0)
                {
                    m_pieces[m_turn][piece] &= ~sourceBitboard;
                    m_pieces[m_turn][piece] |= destinationBitboard;
                    if (piece == 5)
                    {
                        m_halfMoveClock = 0;
                        if ((t_move.destination - t_move.source) == 2 * SLIDE_DIRECTIONS[2 + m_turn * 4])
                        {
                            m_enpassentSquare = (t_move.source + SLIDE_DIRECTIONS[2 + m_turn * 4]);
                        }
                    }
                    break;
                }
            }
            m_allPieces[m_turn] &= ~sourceBitboard;
            m_allPieces[m_turn] |= destinationBitboard;
            for (int piece = 1; piece < 6; ++piece)
            {
                if ((m_pieces[m_opponent][piece] & destinationBitboard) != 0)
                {
                    m_halfMoveClock = 0;
                    m_pieces[m_opponent][piece] &= ~destinationBitboard;
                    break;
                }
            }
            m_allPieces[m_opponent] &= ~destinationBitboard;
        }   
    }
    if (t_move.destination == ((m_opponent * 56) | 7))
    {
        m_canCastle[m_opponent][0] = false;
    }
    else if (t_move.destination == (m_opponent * 56))
    {
        m_canCastle[m_opponent][1] = false;
    }
    
    m_emptyPieces = ~(m_allPieces[WHITE] | m_allPieces[BLACK]);
    m_turn = Color(1 ^ m_turn);
    m_opponent ^= 1;
}

string ChessBoard::getFEN(bool t_checkRepetition)
{
    string fen;
    bool extraSpace = false;
    for (int rank = 7; rank >= 0; --rank)
    {
        int space = 0;
        for (int file = 0; file < 8; ++file)
        {
            extraSpace = true;
            bitboard square = 1ULL << ((rank << 3) + file);
            for (int color = 0; color < 2; ++color)
            {
                if ((m_allPieces[color] & square) != 0)
                {
                    if (space != 0)
                    {
                        fen += ('0' + space);
                    }
                    space = 0;
                    extraSpace = false;
                    for (int piece = 0; piece < 6; ++piece)
                    {
                        if ((m_pieces[color][piece] & square) != 0)
                        {
                            fen += PIECE_CHARS[color * 6 + piece];
                            break;
                        }
                    }
                }
            }
            if (extraSpace)
            {
                ++space;
            }
        }
        if (space != 0)
        {
            fen += ('0' + space);
        }
        fen += '/';
    }
    fen += " ";
    fen += ((m_turn == WHITE) ? "w" : "b");
    fen += " ";
    extraSpace = false;
    for (int color = 0; color < 2; ++color)
    {
        for (int side = 0; side < 2; ++side)
        {
            if (m_canCastle[color][side])
            {
                fen += CASTLE_SIGNS[color][side];
                extraSpace = true;
            }
        }
    }
    if (extraSpace)
    {
        fen += ' ';
    }
    else
    {
        fen += "- ";
    }
    
    if (m_enpassentSquare == -33)
    {
        fen += '-';
    }
    else
    {
        fen += getSquareName(m_enpassentSquare);
    }
    if (!t_checkRepetition)
    {
        fen += " " + std::to_string(m_halfMoveClock) + " " + std::to_string(m_fullMoveCount);
    }
    return fen;
}

bitboard ChessBoard::getPieces(int t_color, int t_piece)
{
    return m_pieces[t_color][t_piece];
}

Color ChessBoard::getTurn()
{
    return m_turn;
}

bool ChessBoard::canCastle(int t_color, int t_side)
{
    return m_canCastle[t_color][t_side];
}

int ChessBoard::getEnpassent()
{
    return m_enpassentSquare;
}

int ChessBoard::getHalfMoveClock()
{
    return m_halfMoveClock;
}

int ChessBoard::getFullMoveCount()
{
    return m_fullMoveCount;
}

bool ChessBoard::insufficentMaterial()
{
    bitboard matingPieces = 0;
    for (int color = 0; color < 2; ++color)
    {
        matingPieces |= m_pieces[color][QUEEN] | m_pieces[color][ROOK] | m_pieces[color][PAWN];
        if (matingPieces != 0)
        {
            return false;
        }
    }
    for (int color = 0; color < 2; ++color)
    {
        matingPieces = m_pieces[color][BISHOP] | m_pieces[color][KNIGHT];
        if ((matingPieces & (matingPieces - 1)) != 0)
        {
            return false;
        }
    }
    return true;
}