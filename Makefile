CC = g++
CFLAGS = -O3
SFMLFLAGS = -lsfml-graphics -lsfml-window -lsfml-system
OUTPUT = chess

$(OUTPUT) : core.o board.o game.o engine.o gui.o main.o
	$(CC) -o $(OUTPUT) core.o board.o game.o engine.o gui.o main.o $(SFMLFLAGS)

core.o : core.cpp core.hpp
	$(CC) $(CFLAGS) -c core.cpp

board.o : board.cpp board.hpp
	$(CC) $(CFLAGS) -c board.cpp

game.o : game.cpp game.hpp
	$(CC) $(CFLAGS) -c game.cpp

engine.o : engine.cpp engine.hpp
	$(CC) $(CFLAGS) -c engine.cpp

gui.o : gui.cpp gui.hpp
	$(CC) $(CFLAGS) -c gui.cpp

main.o : main.cpp
	g++ $(CFLAGS) -c main.cpp

clean :
	rm core.o board.o game.o engine.o gui.o main.o