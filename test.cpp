#include "board.hpp"

long long int perft(const int t_depth, const int n, ChessBoard t_board, Move t_move)
{
    long long int m_perft = 0;
    t_board.playMove(t_move);
    vector<Move> moves = t_board.generateMoves();
    if (n == 1)
        for (int i = 0; i < n; ++ i)
        {
            cout << "-- ";
        }   
    /* 
    if (n == 1)
        cout << getSquareName(t_move.source) << " -> " << getSquareName(t_move.destination)
            << " (" << t_move.source << " -> " << t_move.destination << "): begin\n";
    */
    if (t_depth == 1)
    {
        m_perft = moves.size();
    }
    else
    {
        for (int i = 0; i < moves.size(); ++i)
        {
            m_perft += perft(t_depth - 1, n+1, t_board, moves[i]);
        }
    }
    //t_board.print();
    //cout << "\nPerft depth: " << t_depth << "\nResult: " << m_perft << "\n";
    if (n == 1)
    for (int i = 0; i < n; ++ i)
    {
        cout << "-- ";
    }
    return m_perft;
}
long long int startPerft(const int t_depth
    , ChessBoard t_board = ChessBoard())
{
    long long int m_perft = 0;
    vector<Move> moves = t_board.generateMoves();
    if (t_depth == 1)
    {
        m_perft = moves.size();
        for (int i = 0; i < moves.size(); ++i)
        {
            cout << getSquareName(moves[i].source) << " -> " << getSquareName(moves[i].destination) << "\n";
        }
    }
    else
    {
        for (int i = 0; i < moves.size(); ++i)
        {
            long long int t_perft = perft(t_depth - 1, 1, t_board, moves[i]);
            cout << getSquareName(moves[i].source) << " -> " << getSquareName(moves[i].destination)
            << " (" << moves[i].source << " -> " << moves[i].destination << "): [" << t_perft << "]\n";
            m_perft += t_perft;
        }
    }
    cout << "FEN format: " << t_board.getFEN() << "\nPerft depth: " << t_depth << "\nResult: " << m_perft << "\n";
    return m_perft;
}

int main()
{
    initialize();
    string fen = "rnbqkbnr/pppp1ppp/8/4p3/4P3/5N2/PPPP1PPP/RNBQKB1R/ b KQkq - 1 2";
    //string fen = "rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1";
    ChessBoard board(fen);
    int depth = 5;
    //board.playMove(Move{36, 19});
    //board.playMove(Move{23, 14});
    //board.playMove(Move{32, 25});
    //board.playMove(Move{50, 34});
    //board.playMove(Move{61, 5});
    //board.playMove(Move{14, -3});
    
    board.print();
    vector<Move> moves = board.generateMoves();
    cout << "MOVES:\n";
    /*
    for (int i = 0; i < moves.size(); ++i)
    {
        cout << "\t" << getSquareName(moves[i].source) << " -> " << getSquareName(moves[i].destination) << "\n";
    }
    */
    long long int perft = startPerft(depth, board);
    /*
    for (int i = 0; i < 64; ++i)
    {
        cout << i << '\n';
        printBitboard(PAWN_ATTACKS[BLACK][i]);
    }
    */
    return 0;
}