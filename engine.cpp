#include "engine.hpp"

long long int nodeCount;
// evaluates the board based on value of the pieces on the board (greedy evaluation)
int Engine::evaluate(ChessBoard& t_board, vector<Move>& t_moves)
{
    ++nodeCount;
    vector<int> pieces[2][6];
    for (int color = 0; color < 2; ++color)
    {
        for (int piece = 0; piece < 6; ++piece)
        {
            bitboard pieceBitboard = t_board.getPieces(color, piece);
            while (pieceBitboard != 0)
            {
                pieces[color][piece].push_back(getFirstBit(pieceBitboard));
                pieceBitboard &= pieceBitboard - 1;
            }
        }
    }
    int materialScore[2];
    for (int piece = 1; piece < 6; ++piece)
    {
        materialScore[WHITE] += pieces[WHITE][piece].size() * PIECE_VALUES[piece - 1];
        materialScore[BLACK] += pieces[BLACK][piece].size() * PIECE_VALUES[piece - 1];
    }
    int evaluation = materialScore[WHITE] - materialScore[BLACK];
    return evaluation;
}

int Engine::negamax(ChessBoard& t_board, int t_depth, int t_alpha, int t_beta)
{
    vector<Move> moves = t_board.generateMoves();
    if (moves[0].source == -1)
    {
        if (moves[0].destination == 0)
        {
            return 0;
        }
        else
        {
            return (10000 + t_depth) * amplifier(t_board.getTurn());
        }
    }
    else if ((t_board.getHalfMoveClock() >= 50) || (t_board.insufficentMaterial()))
    {
        return 0;
    }
    else if (t_depth == 0)
    {
        return evaluate(t_board, moves) * amplifier(t_board.getTurn()) + moves.size() * 3;
    }
    else
    {
        int a = -30000;
        for (int i = 0; i < moves.size(); ++i)
        {
            ChessBoard tmpBoard = t_board;
            tmpBoard.playMove(moves[i]);
            int evaluation = -negamax(tmpBoard, t_depth - 1, -t_beta, -t_alpha);
            if (evaluation > a)
            {
                a = evaluation;
            }
            if (evaluation > t_alpha)
            {
                t_alpha = evaluation;
            }
            if (t_alpha >= t_beta)
            {
                break;
            }
        }
        if (t_alpha == -20000)
        {
            cout << "VALUE:" << a << "\n";
        }
        return t_alpha;
    }
}

Move Engine::choose(ChessBoard t_board)
{
    nodeCount = 0;
    vector<Move> moves = t_board.generateMoves();
    int bestMoveIndex = -1;
    int bestScore = -30000;
    int depth = 4;
    int moveCount = t_board.getFullMoveCount();
    vector<int> pieces[2][6];
    for (int color = 0; color < 2; ++color)
    {
        for (int piece = 0; piece < 6; ++piece)
        {
            bitboard pieceBitboard = t_board.getPieces(color, piece);
            while (pieceBitboard != 0)
            {
                pieces[color][piece].push_back(getFirstBit(pieceBitboard));
                pieceBitboard &= pieceBitboard - 1;
            }
        }
    }
    int materialScore[2];
    for (int piece = 1; piece < 6; ++piece)
    {
        materialScore[WHITE] += pieces[WHITE][piece].size() * PIECE_VALUES[piece - 1];
        materialScore[BLACK] += pieces[BLACK][piece].size() * PIECE_VALUES[piece - 1];
    }
    bool endgame = ((materialScore[WHITE] <= 1300) && (materialScore[BLACK]) <= 1300);
    if (endgame)
    {
        depth = 6;
    }
    else if (moveCount >= 25)
    {
        depth = 5;
    }
    for (int i = 0; i < moves.size(); ++i)
    {
        ChessBoard tmpBoard = t_board;
        tmpBoard.playMove(moves[i]);
        long int evaluation = -negamax(tmpBoard, depth - 1, -20000, 20000);
        if (evaluation > bestScore)
        {
            bestMoveIndex = i;
            bestScore = evaluation;
        }
    }

    return moves[bestMoveIndex];
}