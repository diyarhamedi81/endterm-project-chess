/*  piece names will be abbreviated at this point forward:
    white king -> K, black king -> k
    white queen -> Q, black queen -> q
    white rook -> R, black rook -> r
    white bishop -> B, black bishop -> b
    white knight -> N, black knight -> n
    white pawn -> P, black pawn -> p
    In arrays with 2 or more dimensions, 1st dimension determines color */

#ifndef CHESS_CORE_HPP
#define CHESS_CORE_HPP
#include <iostream>
#include <sstream>
#include <cstdint>
#include <string>
#include <vector>
using std::cout;
using std::string;
using std::vector;

enum Piece
{
    KING,
    QUEEN,
    ROOK,
    BISHOP,
    KNIGHT,
    PAWN,
};
enum Color
{
    WHITE,
    BLACK,
    NONE
};

typedef uint64_t bitboard;
const string PIECE_CHARS = "KQRBNPkqrbnp";
const string PIECE_SYMBOLS[] = {"\u2654", "\u2655", "\u2656", "\u2657", "\u2658", "\u2659", "\u265A", "\u265B", "\u265C", "\u265D", "\u265E", "\u265F"};
// indexes used for finding the first set bit
const int index64[64] =
    {
        63, 0, 58, 1, 59, 47, 53, 2,
        60, 39, 48, 27, 54, 33, 42, 3,
        61, 51, 37, 40, 49, 18, 28, 20,
        55, 30, 34, 11, 43, 14, 22, 4,
        62, 57, 46, 52, 38, 26, 32, 41,
        50, 36, 17, 19, 29, 10, 13, 21,
        56, 45, 25, 31, 35, 16, 9, 12,
        44, 24, 15, 8, 23, 7, 6, 5};
// find the first set bit
int getFirstBit(const bitboard);
// shift the bitboard in the specified direction
bitboard shift(const bitboard &, const int);
const bitboard SHIFT_MASKS[8] =
    {
        0xFEFEFEFEFEFEFEFE,
        0xFEFEFEFEFEFEFEFE,
        0xFFFFFFFFFFFFFFFF,
        0x7F7F7F7F7F7F7F7F,
        0x7F7F7F7F7F7F7F7F,
        0x7F7F7F7F7F7F7F7F,
        0xFFFFFFFFFFFFFFFF,
        0xFEFEFEFEFEFEFEFE};
const int SLIDE_DIRECTIONS[8] =
    {
        1,  // right
        9,  // up-right
        8,  // up
        7,  // up-left
        -1, //left
        -9, //down-left
        -8, //down
        -7  //down-right
};
const int PAWN_PROMOTIONS[2][4] = {{8, 16, 24, 32}, {-8, -16, -24, -32}};
// masks for checking if castling is possible
const bitboard CASTLE_EMPTY[2][2] =
    {
        {
            0x0000000000000060,
            0x000000000000000E,
        },
        {0x6000000000000000,
         0x0E00000000000000}};
const bitboard CASTLE_CHECK[2][2] =
    {
        {
            0x0000000000000060,
            0x000000000000000C,
        },
        {0x6000000000000000,
         0x0C00000000000000}};
// king destinations for castling
const int CASTLE_DESTINATIONS[2][2] = {{6, 2}, {62, 58}};
// characters for using in the FEN format
const char CASTLE_SIGNS[2][2] = {{'K', 'Q'}, {'k', 'q'}};
// masks for ranks, files, diagonals & anti-diagonals of the board
const bitboard RANK_MASKS[8] =
    {
        0x00000000000000FF,
        0x000000000000FF00,
        0x0000000000FF0000,
        0x00000000FF000000,
        0x000000FF00000000,
        0x0000FF0000000000,
        0x00FF000000000000,
        0xFF00000000000000};
const bitboard FILE_MASKS[8] =
    {
        0x0101010101010101,
        0x0202020202020202,
        0x0404040404040404,
        0x0808080808080808,
        0x1010101010101010,
        0x2020202020202020,
        0x4040404040404040,
        0x8080808080808080};
const bitboard DIAG_MASKS[15] =
    {
        0x0000000000000001,
        0x0000000000000102,
        0x0000000000010204,
        0x0000000001020408,
        0x0000000102040810,
        0x0000010204081020,
        0x0001020408102040,
        0x0102040810204080,
        0x0204081020408000,
        0x0408102040800000,
        0x0810204080000000,
        0x1020408000000000,
        0x2040800000000000,
        0x4080000000000000,
        0x8000000000000000,
};
const bitboard ANTI_DIAG_MASKS[15] =
    {
        0x0100000000000000,
        0x0201000000000000,
        0x0402010000000000,
        0x0804020100000000,
        0x1008040201000000,
        0x2010080402010000,
        0x4020100804020100,
        0x8040201008040201,
        0x0080402010080402,
        0x0000804020100804,
        0x0000008040201008,
        0x0000000080402010,
        0x0000000000804020,
        0x0000000000008040,
        0x0000000000000080,
};
// precomputed move masks for pawn attacks, knight moves & king moves
extern bitboard PAWN_ATTACKS[2][64];
extern bitboard KNIGHT_MOVES[64];
extern bitboard KING_MOVES[64];
// precompute masks
void initialize();

int bitCount(bitboard);
int amplifier(const int); // return 1 for black, -1 for white

int getRank(const int);
int getFile(const int);
int getDiagonal(const int);
int getAntiDiagonal(const int);
string getSquareName(const int);
int getSquareAddress(string);

struct Move
{
    int source;
    int destination;
};
// show the bitboard in binary form, in the shape of the board
void printBitboard(bitboard);

#endif