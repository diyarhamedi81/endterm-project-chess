#ifndef CHESS_GUI_HPP
#define CHESS_GUI_HPP
#include <SFML/Window.hpp>
#include <SFML/Graphics.hpp>
#include <map>
#include <cmath>
#include "game.hpp"
#include "engine.hpp"

template <typename key, typename resource>
class ResourceManager
{
    public:
    resource& getResource(key);
    void addResource(key, string);
    private:
    std::map<key, resource> m_resources; 
};

extern ResourceManager<string, sf::Texture> textureManager;
extern ResourceManager<string, sf::Font> fontManager;

void loadResources();

class GUIBoard
{
    public:
    GUIBoard(int, int, string = "rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1");
    void setPosition(int = 0, int = 0);
    void update();
    void drawOn(sf::RenderWindow&);
    int checkClick(sf::Vector2i);
    ChessGame& getGame();
    private:
    sf::Sprite m_tiles[8][8];
    vector<sf::Sprite> m_pieces[2][6];
    sf::Sprite m_promotionSquares[2][4];
    sf::Text m_indexes[2][8];
    ChessGame m_game;
    int m_source;
    int m_destination;
    Color m_promotionSide;
    int m_x;
    int m_y;
};

bool checkResult(ChessGame, sf::Text&, int, int);
void playScreen();

#endif