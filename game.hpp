#ifndef CHESS_GAME_HPP
#define CHESS_GAME_HPP
#include "board.hpp"

class ChessGame
{
    public:
    ChessGame(const string = "rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1");
    ~ChessGame();
    ChessBoard getLastBoard();
    vector<Move> getMoves();
    void playMove(Move);
    int getResult();
    private:
    vector<ChessBoard> m_stages;
    vector<Move> m_moves;

    bool threefoldRepetition();
};

#endif