#include "gui.hpp"

template<typename key, typename resource>
resource& ResourceManager<key, resource>::getResource(key t_key)
{
    return m_resources[t_key];
}
template<typename key, typename resource>
void ResourceManager<key, resource>::addResource(key t_key, std::string t_filename)
{
    resource res;
    t_filename = "data/" + t_filename;
    if (!res.loadFromFile(t_filename))
    {
        cout << "failed to load resource from file " << t_filename << "!\n";
        return;
    }
    m_resources.emplace(t_key, res);
}

ResourceManager<string, sf::Texture> textureManager;
ResourceManager<string, sf::Font> fontManager;

void loadResources()
{
    textureManager.addResource("K", "white_king.png");
    textureManager.addResource("Q", "white_queen.png");
    textureManager.addResource("R", "white_rook.png");
    textureManager.addResource("B", "white_bishop.png");
    textureManager.addResource("N", "white_knight.png");
    textureManager.addResource("P", "white_pawn.png");
    textureManager.addResource("k", "black_king.png");
    textureManager.addResource("q", "black_queen.png");
    textureManager.addResource("r", "black_rook.png");
    textureManager.addResource("b", "black_bishop.png");
    textureManager.addResource("n", "black_knight.png");
    textureManager.addResource("p", "black_pawn.png");
    textureManager.addResource("tile", "tile.png");

    fontManager.addResource("default", "ArialUnicodeMS.ttf");
}

GUIBoard::GUIBoard(int t_x, int t_y, string t_fen)
:m_x(t_x), m_y(t_y), m_promotionSide(NONE), m_source(-2), m_destination(-33)
{
    m_game = ChessGame(t_fen);
    for (int rank = 0; rank < 8; ++rank)
    {
        for (int file = 0; file < 8; ++file)
        {
            m_tiles[rank][file].setTexture(textureManager.getResource("tile"));
            m_tiles[rank][file].setPosition(64 * file + m_x, 448 - 64 * rank + m_y);
        }
    }
    for (int i = 0; i < 2; ++i)
    {
        for (int j = 0; j < 8; ++j)
        {
            m_indexes[i][j].setFont(fontManager.getResource("default"));
            m_indexes[i][j].setFillColor(sf::Color::Black);
            m_indexes[i][j].setCharacterSize(32);
        }
    }
    for (int color = 0; color < 2; ++color)
    {
        for (int piece = 1; piece <= 4; ++piece)
        {
            m_promotionSquares[color][piece - 1].setTexture(textureManager.getResource(string(1, PIECE_CHARS[color * 6 + piece])));
            m_promotionSquares[color][piece - 1].setPosition(512 + m_x, 64 * (color * 4 + piece - 1) + m_y);
        }
    }
    for (int i = 0; i < 8; ++i)
    {
        m_indexes[0][i].setString(sf::String((char)('8' - i)));
        m_indexes[0][i].setPosition(m_x - 40, m_y + 12 + i * 64);
        m_indexes[1][i].setString(sf::String((char)('a' + i)));
        m_indexes[1][i].setPosition(m_x + 24 + i * 64, m_y + 520);
    }
}
void GUIBoard::setPosition(int t_x, int t_y)
{
    m_x = t_x;
    m_y = t_y;
}
void GUIBoard::update()
{
    vector<int> destinationSquares;
    vector<Move> moves = m_game.getLastBoard().generateMoves();
    for (int i = 0; i < moves.size(); ++i)
    {
        if (moves[i].source == m_source)
        {
            if (moves[i].destination >= 64)
            {
                moves[i].destination = 56 | (moves[i].destination & 7);
            }
            else if (moves[i].destination < 0)
            {
                moves[i].destination = 8 - (-moves[i].destination & 7);
            }
            destinationSquares.push_back(moves[i].destination);
        }
    }
    for (int rank = 0; rank < 8; ++rank)
    {
        for (int file = 0; file < 8; ++file)
        {
            if (((rank + file) % 2) == 0)
            {
                m_tiles[rank][file].setColor(sf::Color(40, 40, 40));
            }
            else
            {
                m_tiles[rank][file].setColor(sf::Color(255, 255, 255));
            }
            if ((rank * 8 + file) == m_source)
            {
                m_tiles[rank][file].setColor(sf::Color(255, 255, 0));
            }
            else
            {
                for (int i = 0; i < destinationSquares.size(); ++i)
                {
                    if ((rank * 8 + file) == destinationSquares[i])
                    {
                        m_tiles[rank][file].setColor(sf::Color(40, 255, 40));
                    }
                }
            } 
            m_tiles[rank][file].setPosition(64 * file + m_x, 448 - 64 * rank + m_y);
        }
    }
    for (int color = 0; color < 2; ++color)
    {
        for (int piece = 0; piece < 6; ++piece)
        {
            m_pieces[color][piece].clear();
            bitboard piecesBitboard = m_game.getLastBoard().getPieces(color, piece);
            for (int square = 0; square < 64; ++square)
            {
                if (((1ULL << square) & piecesBitboard) != 0)
                {
                    m_pieces[color][piece].push_back(sf::Sprite());
                    m_pieces[color][piece].back().setTexture
                    (textureManager.getResource(string(1, PIECE_CHARS[color * 6 + piece])));
                    m_pieces[color][piece].back().setPosition(64 * getFile(square) + m_x + 2, 448 - 64 * getRank(square) + m_y + 2);
                }
            }
        }
    }
}
void GUIBoard::drawOn(sf::RenderWindow& t_window)
{
    for (int rank = 0; rank < 8; ++rank)
    {
        for (int file = 0; file < 8; ++file)
        {
            t_window.draw(m_tiles[rank][file]);
        }
    }
    for (int i = 0; i < 2; ++i)
    {
        for (int j = 0; j < 8; ++j)
        {
            t_window.draw(m_indexes[i][j]);
        }
    }
    for (int color = 0; color < 2; ++color)
    {
        for (int piece = 0; piece < 6; ++piece)
        {
            for (int i = 0; i < m_pieces[color][piece].size(); ++i)
            {
                t_window.draw(m_pieces[color][piece][i]);
            }
        }
        if (m_promotionSide == Color(color))
        {
            
            for (int piece = 1; piece <= 4; ++piece)
            {
                t_window.draw(m_promotionSquares[color][piece - 1]);
            }    
        }
    }
}

int GUIBoard::checkClick(sf::Vector2i t_mousePosition)
{
    int result = 0;
    int x = std::floor((t_mousePosition.x - m_x) / 64.0);
    int y = 7 - std::floor((t_mousePosition.y - m_y) / 64.0);
    ChessBoard board = m_game.getLastBoard();
    vector<Move> moves = board.generateMoves();
    if ((x >= 0) && (x <= 7) && (y >= 0) && (y <= 7))
    {
        for (int i = 0; i < moves.size(); ++i)
        {
            if (moves[i].source == (y * 8 + x))
            {
                m_source = y * 8 + x;
                m_destination = -33;
                m_promotionSide = NONE;
                update();
                break;
            }
        }    
    }
    if (m_promotionSide != NONE)
    {
        if (x == 8)
        {
            bool play = false;
            if ((y >= 4) && (y < 8) && (m_promotionSide == WHITE))
            {
                m_destination += 8 * (8 - y);
                play = true;
            }
            else if ((y >= 0) && (y < 4) && (m_promotionSide == BLACK))
            {
                m_destination -= 8 * (4 - y);
                play = true;
            }
            if (play)
            {
                m_game.playMove(Move{m_source, m_destination});
                m_source = -2;
                m_destination = -33;
                m_promotionSide = NONE;
                update();
                result = 1;  
            }
        }
    }
    if ((x >= 0) && (x <= 7) && (y >= 0) && (y <= 7))
    {
        for (int i = 0; i < moves.size(); ++i)
        {
            if ((m_source == moves[i].source))
            {
                if ((y * 8 + x) != moves[i].destination)
                {
                    if ((moves[i].destination >= 64) && ((56 | (moves[i].destination & 7)) == (y * 8 + x)))
                    {
                        m_promotionSide = WHITE;
                        m_destination = y * 8 + x;
                        update();
                        break;
                    }
                    else if ((moves[i].destination < 0) && ((8 - (-moves[i].destination & 7)) == (y * 8 + x)))
                    {
                        m_promotionSide = BLACK;
                        m_destination = y * 8 + x;                        
                        update();
                        break;
                    }
                }
                else
                {
                    m_destination = moves[i].destination;
                    result = 1;
                    m_game.playMove(Move{m_source, m_destination});
                    m_source = -2;
                    m_destination = -33;
                    update();
                    break;
                }
            }
        }
    }
    return result;
}

ChessGame& GUIBoard::getGame()
{
    return m_game;
}

bool checkResult(ChessGame t_game, sf::Text& t_text, int t_x, int t_y)
{
    bool finished;
    int gameResult = t_game.getResult();
    switch (gameResult)
    {
        case 1:
        {
            t_text.setPosition(t_x + 96, t_y - 48);
            string winningSide = (t_game.getLastBoard().getTurn() == BLACK) ? "White" : "Black";
            t_text.setString(sf::String("It's Checkmate. " + winningSide + " wins!"));
            finished = true;
            break;
        }
        case -1:
        {
            t_text.setPosition(t_x + 84, t_y - 48);
            t_text.setString(sf::String("It's Stalemate. Nobody wins!"));
            finished = true;
            break;
        }
        case -2:
        {
            t_text.setPosition(t_x + 36, t_y - 48);
            t_text.setString(sf::String("It's Draw because of fifty moves rule!"));
            finished = true;
            break;
        }
        case -3:
        {
            t_text.setPosition(t_x + 10, t_y - 48);
            t_text.setString(sf::String("It's Draw because of threefold repetition!"));
            finished = true;
            break;
        }
        case -4:
        {
            t_text.setPosition(t_x + 10, t_y - 48);
            t_text.setString(sf::String("It's Draw because of insufficent material!"));
            finished = true;
            break;
        }
        default:
        {
            t_text.setPosition(t_x + 160, t_y - 48);
            string movingSide = (t_game.getLastBoard().getTurn() == WHITE) ? "White" : "Black";
            t_text.setString(sf::String("It's " + movingSide + "'s turn!"));
            finished = false;
            break;
        }
    }
    return finished;
}

void playScreen()
{
    int selectedSquare = -2;
    int x = 64, y = 64;
    sf::RenderWindow window(sf::VideoMode(640, 678), "CHESS", sf::Style::Close | sf::Style::Titlebar);
    window.setFramerateLimit(60);
    GUIBoard board(x, y);
    ChessGame &game = board.getGame();
    board.update();
    sf::Text text;
    text.setFillColor(sf::Color::Black);
    text.setCharacterSize(24);
    text.setFont(fontManager.getResource("default"));
    text.setStyle(sf::Text::Bold);
    int clickResult;
    bool playing = true;
    bool finished = checkResult(game, text, x, y);
    Engine engine;
    while (window.isOpen())
    {
        if (!playing && !finished)
        {
            Move move = engine.choose(game.getLastBoard());
            game.playMove(move);
            board.update();
            playing = true;
            finished = checkResult(game, text, x, y);
        }
        sf::Event event;
        while (window.pollEvent(event))
        {
            if (event.type == sf::Event::Closed)
                window.close();
			else if (event.type == sf::Event::MouseButtonPressed)
            {
                if (event.mouseButton.button == sf::Mouse::Left)
                {
                    if (playing && !finished)
                    {
                        clickResult = board.checkClick(sf::Mouse::getPosition(window));
                        if (clickResult == 1)
                        {
                            playing = false;
                        }
                        finished = checkResult(game, text, x, y);
                    }
                }
            }
        }
		window.clear(sf::Color(240, 240, 240));
        window.draw(text);
        board.drawOn(window);
        window.display();
    }
}